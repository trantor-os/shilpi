# Shilpi - Building Trantor OS images
#
# Copyright 2020 Raghu Kaippully.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#

export TARGET  := i586-elf
export SYSROOT := $(abspath ./install)

TEST_HD   := test/hd0.img
TEST_VDI  := test/hd0.vdi

all: install-trantor

$(TEST_HD): TRANTOR/GRUB/GRUB.CFG
	scripts/make-disk $@

$(SYSROOT)/TRANTOR/BOOT/KERNEL.SYS: ../kernel/build/kernel.sys
	mkdir -p $(dir $@)
	cp $^ $@

../kernel/build/kernel.sys: FORCE
	$(MAKE) -C ../kernel

FORCE:


.PHONY: install-trantor qemu-test bochs-test vbox-test

install-trantor: $(TEST_HD) TRANTOR/BOOT/BOOTCFG.INI $(SYSROOT)/TRANTOR/BOOT/KERNEL.SYS
	scripts/install-trantor $(TEST_HD)

qemu-test: install-trantor
	qemu-system-i386 --enable-kvm -cpu pentium -m 4M -drive file=$(TEST_HD),index=0,media=disk,format=raw -vga std

bochs-test: install-trantor
	tools/bin/bochs -q -f test/bochsrc

$(TEST_VDI): install-trantor
	$(RM) -f $@
	scripts/make-vbox-vm $^ $@

vbox-test: $(TEST_VDI)
	VBOX_GUI_DBG_ENABLED=true VBoxManage startvm TrantorOS


.PHONY: clean
clean:
	$(RM) -rf $(TEST_HD) $(TEST_VDI) test/mnt test/device.map $(SYSROOT)
